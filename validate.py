

# check valid email or not
import re
email = input("enter the mail")

regex = "^\w([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"

if (re.search(regex,email)):
    print("valid email id")
else:
    print("invalid email id")   


# Validations for characters which are only alphabets
regex = "[a-zA-Z ]*$"
str1 = "pythonLANGUAGE"
if (re.search(regex,str1)):
    print("valid ")
else:
    print("invalid")  



# validate for character which are only special symbols

regex = "[^\w\s]"
strr = "%^%$##"
if (re.search(regex,strr)):
    print("valid string")
else:
    print("invalid ")

# check characters are only digit
regex = "^[0-9]*$"
num = "345353"
if (re.search(regex,num)):
    print("valid input ")
else:
    print("invalid ")




from datetime import date
def age_cal(dob):
    today = date.today()
    return today.year - dob.year - ((today.month , today.day) < (dob.month , dob.day))
age = age_cal(date(1996 ,9 ,8))
if age > 18:
    print("valid user")
else:
    print("invalid user")


# phone number validations
regex = "^[7-9]\d{9}$"
phone = "7206582406"
if (re.search(regex,phone)):
    print("valid phone number")
else:
    print("invalid number")   