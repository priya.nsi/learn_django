
# itrating on dictionary
dic ={'diya':456345345,'riya':56454634746,'niya':56546466756,'priya':4576787877}
for name , number in dic.items():
    print('name and number is %s %d' %(name,number))

# dictionary line by line print
dic = {'priya':{'id':1601,'dep':'python'},
      'riya':{'id':4532,'dep':'php'}}
for emp in dic:
    print(emp)
    for info in dic[emp]:
        print(info ,':', dic[emp][info])

# unique values in dictionary
list1 = [{1:'python'},{2:'php'},{3:'C+'},{1:'C'},{3:'php'}]
print(list1)
u_value = set(val for dic in list1 for val in dic.values())
print(u_value)

# items in dictionary values that is list
dic = {'priya':['mark','mark2','mark3'],'riya':['mark1','mark2','mark3']}
count = sum(map(len,dic.values()))
print(count)


