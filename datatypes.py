# itrating on dictionary
dic ={'diya':456345345,'riya':56454634746,'niya':56546466756,'priya':4576787877}
for name , number in dic.items():
    print('name and number is %s %d' %(name,number))




# dictionary line by line print
dic = {'priya':{'id':1601,'dep':'python'},
      'riya':{'id':4532,'dep':'php'}}
for emp in dic:
    print(emp)
    for info in dic[emp]:
        print(info ,':', dic[emp][info])




# unique values in dictionary
list1 = [{1:'python'},{2:'php'},{3:'C+'},{1:'C'},{3:'php'}]
print(list1)
u_value = set(val for dic in list1 for val in dic.values())
print(u_value)




# items in dictionary values that is list
dic = {'priya':['mark','mark2','mark3'],'riya':['mark1','mark2','mark3']}
count = sum(map(len,dic.values()))
print(count)

# check valid email or not
import re
email = input("enter the mail")

regex = "^\w([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$"

if (re.search(regex,email)):
    print("valid email id")
else:
    print("invalid email id")   


# Validations for characters which are only alphabets
regex = "[a-zA-Z ]*$"
str1 = "pythonLANGUAGE"
if (re.search(regex,str1)):
    print("valid ")
else:
    print("invalid")  



# validate for character which are only special symbols

regex = "[^\w\s]"
strr = "%^%$##"
if (re.search(regex,strr)):
    print("valid string")
else:
    print("invalid ")

# check characters are only digit
regex = "^[0-9]*$"
num = "345353"
if (re.search(regex,num)):
    print("valid input ")
else:
    print("invalid ")


