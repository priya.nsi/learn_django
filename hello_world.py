# program to check similar key in two different dictionay 
dic = {1:"ram",2:"riya",3:"woo"}
dic2 = {3:"hi",4:"yoo",1:"ram"}
for key ,value in set(dic.items()) & set(dic2.items()):
  print("the key which is similar in both dictionary is %s: %s " % (key,value))


# program to reverse tuple and find index value of p
tup = (" its python language") 
tup_rev = reversed(tup)
print(tup_rev)
print(tup.index("p"))


#example of list performing some fuction
list_data = ["riya","diya","neha","rahul","jack","mack"]
for i in list_data:
  print(i)
print(list_data.append('deep'))
print(list_data.sort())
print(list_data.insert(2,'priya'))

# program of set  intersaction and union of two sets

x = {"python","java","c+","c"}
y = {"hindi","english","python"}
z = {"hi","hloo","woo"}
inter = x & y
print("intersaction of x and y",inter)
un = x | y
print("union of x or  y",un)
print("symantic difference of x and y", x ^ y)
print("difference in x - y",x-y)
print(x.isdisjoint(z))


# list practice 
l = []
for i in range(1,10):
  l.append(i**2)
print(l[5:])

# list of character convert into string
l = ["h","l","o"]
str = "".join(l)
print(str)
 
#  random number from list
import random
list1 = [1,2,3,45,6]
r = random.choice(list1)
print(r)


#list split into different variable
my_list = [("blk","#44556","4"),("red"),("red","#124","5")]
var, var1, var2 = my_list
print(var)
print(var1)
print(var2)
