urls.py
from django.conf.urls import  url
from django.urls import path
from django.contrib import admin
from .views import *

app_name="myapp"

urlpatterns = [
    path(r'page/',page,name = ' page'),
    path(r'<int:question_id>/detail/',detail,name = 'detail'),
    path(r'<int:question_id>/results/',results, name='results'),
    path(r'<int:question_id>/vote/',vote, name='vote'),
    

]


from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from .models import choice, question


views.py

def page(request):
    latest_question_list = question.objects.order_by('-pub_date')[:2]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'temp.html', context)
    
def detail(request,question_id):
    #return HttpResponse("user looking for question this {0}.".format(question_id))
    try:
        quest = question.objects.get(pk=question_id)
    except quest.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request,'detail.html', {'question': quest})
def results(request, question_id):
    # response = "You're looking at the results of question %s."
    # return HttpResponse(response % question_id)
    questions = get_object_or_404(question, pk=question_id)
    return render(request, 'result.html', {'question': questions})

def vote(request, question_id):
    # return HttpResponse("You're voting on question %s." % question_id)   
    questions = get_object_or_404(question, pk=question_id)
    try:
        selected_choice = questions.choice_set.get(pk=request.POST['choice'])
    except (KeyError, choice.DoesNotExist):
        
        return render(request, 'detail.html', {
            'question': questions,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
       
        return HttpResponseRedirect(reverse('myapp:results', args=(questions.id,))) 


     


# def result():
#    response = "the result for your response question this %$"
#    return HttpResponse(" response %$ question_id")    


models.py

from django.db import models
import datetime
from django.utils import timezone

class question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

class choice(models.Model):
    quest = models.ForeignKey(question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

admin.py

from django.contrib import admin


from .models import *
# Register your models here.

admin.site.register(question)
admin.site.register(choice)


# class AuthorAdmin(admin.ModelAdmin):
#     pass
# admin.site.register(Author, AuthorAdmin)


detail.html
<!-- <h1>{{ question.question_text }}</h1> -->
<!-- <ul> --> 
<!-- {% for choice in question.choice_set.all %} -->
    <!-- <li>{{ choice.choice_text }}</li> -->
<!-- {% endfor %} -->
<!-- </ul> -->

<h1>{{ question.question_text }}</h1>

{% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}

<form action="{% url 'myapp:vote' question.id %}" method="post">
{% csrf_token %}
{% for choice in question.choice_set.all %}
    <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
    <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
{% endfor %}
<input type="submit" value="Vote">
</form>

result.html

<h1>{{ question.question_text }}</h1>

<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }} -- {{ choice.votes }} vote{{ choice.votes|pluralize }}</li>
{% endfor %}
</ul>


temp.html

{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
    <li><a href="{{ question.id }}/">{{ question.question_text }}</a></li>

    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}

<a href="{% url 'myapp:detail' question.id %}">Vote again?</a>



