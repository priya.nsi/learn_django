def div(a,b):
    print(a/b)
def deco_div(func):
    def second_div(a,b):
        if a<b:
            a,b = b,a
            return func(a,b)
    return second_div
div = deco_div(div) 
div(2,4)           



# singleton example




class singleton:
    _instance  = None
    def getInstance():
        if singleton._instance == None:
            singleton()
        return singleton._instance
    def _init_(self):
        if singleton._instance != None:
            raise Exception("this is singleton class")
        else:
            singleton._instance = self  

obj = singleton()
print(obj) 
obj = singleton.getInstance()
print(obj)
     
     
# example 2 for singleton

class Singleton(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Singleton, cls).__new__(cls)
        return cls.instance 

obj1 = singleton()
print(obj1)
       
     